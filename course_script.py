
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "magellan2.settings")
django.setup()

from users.models import Course
import cPickle

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def writing():
	print bcolors.OKBLUE + 'begin writing' + bcolors.ENDC
	f = open('save.p', 'wb')
	cPickle.dump(Course.objects.order_by('course_code'), f)
	f.close()
	print bcolors.OKGREEN + 'Number of courses written to file save.p: %d' % Course.objects.count() + bcolors.ENDC
	print bcolors.OKBLUE + 'end writing' + bcolors.ENDC

def loading():
	print bcolors.OKBLUE + 'begin loading' + bcolors.ENDC
	Course.objects.all().delete()
	f = open('save.p', 'rb')
	obj = cPickle.load(f)
	f.close()

	load_count = 0
	for course in obj:
		c = Course(course_code=course.course_code,
				   course_term=course.course_term,
				   course_title=course.course_title,
				   course_description=course.course_description,
				   course_prerequisites=course.course_prerequisites,
				   course_corequisites=course.course_corequisites,
				   course_exclusions=course.course_exclusions,
				   course_credit_weight=course.course_credit_weight,
				   course_math=course.course_math,
				   course_ns=course.course_ns,
				   course_cs=course.course_cs,
				   course_es=course.course_es,
				   course_ed=course.course_ed,
				   course_area=course.course_area)
		c.save()
		load_count += 1

	print bcolors.OKGREEN + 'Number of courses loaded from database: %d' % load_count + bcolors.ENDC
	print bcolors.OKBLUE + 'end loading' + bcolors.ENDC

def deleting():
	print bcolors.OKBLUE + 'begin deleting' + bcolors.ENDC
	Course.objects.all().delete()
	print bcolors.OKBLUE + 'end deleting' + bcolors.ENDC

def show_current_data():
	print bcolors.OKBLUE + 'begin showing current data' + bcolors.ENDC

	current_course_list = Course.objects.order_by('course_code')
	for course in current_course_list:
		print course.course_title

	print bcolors.OKGREEN + 'Number of courses in database: %d' % Course.objects.count() + bcolors.ENDC
	print bcolors.OKBLUE + 'end showing current data' + bcolors.ENDC

var = raw_input("\nChoose option \n\n(S) Save data to file \n(L) Load data from file to database \n(D) Delete the current database\n(C) Show value in current database\n(Q) Quit / Exit\n")

while True:
	if var.lower() == 's' or var.lower() == 'save':
		writing()
	elif var.lower() == 'l' or var.lower() == 'load':
		loading()
	elif var.lower() == 'd' or var.lower() == 'delete':
		deleting()
	else:
		show_current_data()
	var = raw_input("\nChoose option \n\n(S) Save data to file \n(L) Load data from file to database \n(D) Delete the current database\n(C) Show value in current database\n(Q) Quit / Exit\n\n")
	if var.lower() == 'q' or var.lower() == 'exit':
		break




