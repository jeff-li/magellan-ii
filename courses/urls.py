from django.conf.urls import patterns, url

from courses import views

urlpatterns = patterns('',
    url(r'^$', views.courses, name='courses'),
    url(r'^(?P<course_id>\d+)/$', views.course_detail, name='course_detail'),
    url(r'^add_course/(?P<profile_id>\d+)/(?P<term_id>\d+)/$', views.add_course, name='add_course'),
    url(r'^add_course_action/(?P<profile_id>\d+)/(?P<term_id>\d+)/$', views.add_course_action, name='add_course_action'),
    url(r'^remove_course/(?P<profile_id>\d+)/(?P<term_id>\d+)/$', views.remove_course, name='remove_course'),
    url(r'^remove_course_action/(?P<profile_id>\d+)/(?P<term_id>\d+)/$', views.remove_course_action, name='remove_course_action'),
    url(r'^s/$', views.search_course, name='search_course')
)
