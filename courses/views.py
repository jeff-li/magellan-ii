from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required

from users.models import Course, Student
from profiles.models import Profile, Term

from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.http import Http404 


# Create your views here.


@login_required
def courses(request):
    latest_course_list = Course.objects.order_by('course_code')
    top_div = [i * 4 + 1 for i in range(0, 20)]
    bottom_div = [i * 4 for i in range(0, 20) if i > 0]

    latest_course = list(latest_course_list.values_list())
    new_list = []

    # Get query from browser
    query = request.GET.get('q')

    # Create a dictionary for query
    course = {}
    records = []
    results = []

    for item in latest_course_list:
        course['code'] = item.course_code
        course['title'] = item.course_title
        course['term'] = item.course_term
        course['area'] = item.course_area
        course['description'] = item.course_description
        course['prerequisites'] = item.course_prerequisites
        course['au'] = {'ns':item.course_ns, 
                        'cs':item.course_cs,
                        'es':item.course_es,
                        'ed':item.course_ed,
                        'math':item.course_math}
        records.append(course)
        course = {}

    if query:
        for record in records:
            if query.lower() in record['code'].lower():
                results.append(record)
            elif query.lower() in record['title'].lower():
                results.append(record)

    for i in latest_course:
        i = list(i)

        areas = str(i[14]).split(', ')
        i.append(areas)
        new_list.append(i)

    latest_course = new_list

    # AJAX
    args = {}
    args.update(csrf(request))

    context = {'latest_course_list': latest_course_list, 
               'top_div': top_div, 
               'bottom_div': bottom_div,
               'latest_course':latest_course,
               'records':records,
               'results':results,
               'args':args
                }
    return render(request, 'courses.html', context)

from django.core.context_processors import csrf
from django.shortcuts import render_to_response
from django.db.models import Q
def search_course(request):
    if request.method == "POST":
        query = request.POST["search_text"]
    else:
        query = ''

    # courses = Course.objects.filter(course_code__contains=query)
    # courses = Course.objects.order_by('course_code')
    courses = Course.objects.filter(
            Q(course_code__contains=query) | Q(course_title__contains=query)
        )


    # return HttpResponse(json.dumps(response_dict), content_type="application/javascript")
    return render_to_response('ajax_search.html', {'courses':courses})

# def course_api(request):
#     test = request.POST.get("courseCode","")

#     print "First "

#     response_data = {}
#     try:
#         response_data['result'] = 'Writing the code was a success!'
#         response_data['message'] = test
#     except:
#         response_data['result'] = 'Oh No!'
#         response_data['message'] = "The Subprocess module did not run the script correctly"

#     return HttpResponse(json.dumps(response_data), content_type="application/json")

@login_required
def course_detail(request, course_id):
    course = get_object_or_404(Course, pk=course_id)
    return render(request, 'course_detail.html', {'course': course})


@login_required
def add_course(request, profile_id, term_id):
    current_student = request.user.student
    try:
        current_profile = current_student.profile_set.get(pk=profile_id)
    except Profile.DoesNotExist:
        raise Http404("Profile ID does not exist for this student")
    try:
        current_term = current_profile.term_set.get(pk=term_id)
    except Term.DoesNotExist:
        raise Http404("Term ID does not exist for this profile")
    list_of_courses_taken = current_term.courses.all()
    list_of_all_courses = Course.objects.all()
    courses_not_currently_taken = list(set(list_of_all_courses) - set(list_of_courses_taken))
    # Note: in the real implementation, we should allow them to take courses twice, but maybe forbid
    #		taking a course twice in the same term.
    context = {'courses_not_currently_taken': courses_not_currently_taken, 'profile_id': profile_id,'term_id':term_id }
    return render(request, 'courses/add_course.html', context)


@login_required
def add_course_action(request, profile_id, term_id):
    current_student = request.user.student
    try:
        current_profile = current_student.profile_set.get(pk=profile_id)
    except Profile.DoesNotExist:
        raise Http404("Profile ID does not exist for this student")
    try:
        current_term = current_profile.term_set.get(pk=term_id)
    except Term.DoesNotExist:
        raise Http404("Term ID does not exist for this profile")
    try:
        selected_course = Course.objects.get(id=request.POST['course_choice'])
        year = request.POST['term_choice']
    except (KeyError, Course.DoesNotExist):
        # TODO: find better way than reproducing entire page again
        list_of_courses_taken = current_term.courses.all()
        list_of_all_courses = Course.objects.all()
        courses_not_currently_taken = list(set(list_of_all_courses) - set(list_of_courses_taken))
        context = {'courses_not_currently_taken': courses_not_currently_taken, 'error_message': "No course selected", 'profile_id': profile_id,'term_id':term_id}
        return render(request, 'courses/add_course.html', context)
    else:
        # add the course to the current student
        current_term.courses.add(selected_course)

        return HttpResponseRedirect(reverse('profiles:view_current_profile', args=(profile_id,)))


@login_required
def remove_course(request, profile_id, term_id):
    current_student = request.user.student    
    try:
        current_profile = current_student.profile_set.get(pk=profile_id)
    except Profile.DoesNotExist:
        raise Http404("Profile ID does not exist for this student")
    try:
        current_term = current_profile.term_set.get(pk=term_id)
    except Term.DoesNotExist:
        raise Http404("Term ID does not exist for this profile")
    list_of_courses_taken = current_term.courses.all()
    # Note: in the real implementation, we should allow them to take courses twice, but maybe forbid
    # taking a course twice in the same term.
    context = {'list_of_courses_taken': list_of_courses_taken, 'profile_id': profile_id, 'term_id': term_id}
    return render(request, 'courses/remove_course.html', context)


@login_required
def remove_course_action(request, profile_id, term_id):
    current_student = request.user.student
    try:
        current_profile = current_student.profile_set.get(pk=profile_id)
    except Profile.DoesNotExist:
        raise Http404("Profile ID does not exist for this student")
    try:
        current_term = current_profile.term_set.get(pk=term_id)
    except Term.DoesNotExist:
        raise Http404("Term ID does not exist for this profile")
    try:
        selected_course = current_term.courses.get(id=request.POST['course_choice'])
    except (KeyError, Course.DoesNotExist):
        # TODO: find better way than reproducing entire page again
        list_of_courses_taken = current_profile.staging_list.all()
        context = {'list_of_courses_taken': list_of_courses_taken, 'error_message': "No course selected", 'profile_id': profile_id, 'term_id': term_id}
        return render(request, 'courses/remove_course.html', context)
    else:
        # add the course to the current student
        current_term.courses.remove(selected_course)
        return HttpResponseRedirect(reverse('profiles:view_current_profile', args=(profile_id,)))

