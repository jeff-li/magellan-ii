from django.conf.urls import patterns, include, url
from django.contrib import admin

from users.models import Course

urlpatterns = patterns('',

    # URL for the index page:
    url(r'^$', 'users.views.index', name='index'),
    url(r'^home$', 'users.views.home', name='home'),

    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}, name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),

    # Include URLs from all apps
    url(r'^users/', include('users.urls')),
    url(r'^profiles/', include('profiles.urls', namespace='profiles')),
    url(r'^courses/', include('courses.urls', namespace='courses')),

    # Include URLs from all apps
    url(r'^admin/', include(admin.site.urls)),

)
