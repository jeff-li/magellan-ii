
from django.contrib import admin
from profiles.models import Profile,Term

# Register your models here.
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('student',
                    'profile_name')

class TermAdmin(admin.ModelAdmin):
    list_display = ('profile',
                    'year',
                    'season'
                    )

admin.site.register(Profile, ProfileAdmin)
admin.site.register(Term,TermAdmin)
