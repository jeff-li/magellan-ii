# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LockedInList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('section', models.CharField(max_length=6)),
                ('course', models.ForeignKey(to='users.Course')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('profile_name', models.CharField(max_length=200)),
                ('student', models.ForeignKey(to='users.Student')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StagingList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('selected', models.BooleanField(default=False)),
                ('course', models.ForeignKey(to='users.Course')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Term',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.DecimalField(default=2010, max_digits=4, decimal_places=0)),
                ('season', models.CharField(default=b'FALL', max_length=10)),
                ('profile', models.ForeignKey(to='profiles.Profile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='staginglist',
            name='term',
            field=models.ForeignKey(to='profiles.Term'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='lockedinlist',
            name='term',
            field=models.ForeignKey(to='profiles.Term'),
            preserve_default=True,
        ),
    ]
