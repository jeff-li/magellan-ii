# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lockedinlist',
            name='course',
        ),
        migrations.RemoveField(
            model_name='lockedinlist',
            name='term',
        ),
        migrations.DeleteModel(
            name='LockedInList',
        ),
        migrations.RemoveField(
            model_name='staginglist',
            name='course',
        ),
        migrations.RemoveField(
            model_name='staginglist',
            name='term',
        ),
        migrations.DeleteModel(
            name='StagingList',
        ),
        migrations.RemoveField(
            model_name='term',
            name='profile',
        ),
        migrations.DeleteModel(
            name='Term',
        ),
    ]
