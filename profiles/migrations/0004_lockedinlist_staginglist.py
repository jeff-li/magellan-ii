# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
        ('profiles', '0003_term'),
    ]

    operations = [
        migrations.CreateModel(
            name='LockedInList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('section', models.CharField(max_length=6)),
                ('course', models.ForeignKey(to='users.Course')),
                ('term', models.ForeignKey(to='profiles.Term')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StagingList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('selected', models.BooleanField(default=False)),
                ('course', models.ForeignKey(to='users.Course')),
                ('term', models.ForeignKey(to='profiles.Term')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
