
from django.db import models
from django.contrib.auth.models import User
from users.models import Course, Student


class Profile(models.Model):
    student = models.ForeignKey(Student)
    profile_name = models.CharField(max_length=200)
    #staging_list = models.ManyToManyField(Course, related_name='profile_staging_list' ,through='StagingList', through_fields=('profile', 'course'), blank=True)
    #locked_in_list = models.ManyToManyField(Course, related_name='profile_locked_in_list', through='LockedInList', through_fields=('profile', 'course'), blank=True)

    def __unicode__(self):
        return self.profile_name

class Term(models.Model):
    profile = models.ForeignKey(Profile)
    year = models.DecimalField(max_digits=4, decimal_places=0, default=2010)
    season=models.CharField(max_length=10,default='FALL')
    courses = models.ManyToManyField(Course)

    def __unicode__(self):
        return self.profile.profile_name + " "+ str(self.year)+ " " + self.season


class StagingList(models.Model):
  #  class Meta:
   #     auto_created = True
    #term = models.ForeignKey(Term)
    course = models.ForeignKey(Course)
    selected = models.BooleanField(default=False)
    #term = models.IntegerField(default = 0)

    def __unicode__(self):
        return self.term.profile.profile_name+ " "+ str(self.term.year)+ " " + self.term.season + " "+ self.course.course_code + " " + self.selected



class LockedInList(models.Model):
    #class Meta:
     #   auto_created = True
    term = models.ForeignKey(Term)
    course = models.ForeignKey(Course)
    section = models.CharField(max_length=6)
    def __unicode__(self):
        return  self.term.profile.profile_name+ " "+ str(self.term.year)+ " " + self.term.season + " " + self.course.course_code + " " + self.section
