from django.conf.urls import patterns, url

from profiles import views

urlpatterns = patterns('',
    url(r'^$', views.profiles, name='profiles'),
    url(r'^requirements', views.requirements, name='requirements'),
    url(r'^(?P<profile_id>\d+)/$', views.profile_detail, name='profile_detail'),
    url(r'^view_profiles$', views.view_profiles, name='view_profiles'),
    url(r'^view_current_profile/(?P<profile_id>\d+)/$', views.view_current_profile, name='view_current_profile'),
    url(r'^create_profile$', views.create_profile, name='create_profile'),
    url(r'^delete_profile/(?P<profile_id>\d+)/$', views.delete_profile, name='delete_profile'),
    url(r'^create_term/(?P<profile_id>\d+)/$', views.create_term, name='create_term')
)


