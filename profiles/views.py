from django.shortcuts import render
from django.shortcuts import render, get_object_or_404
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.core.context_processors import csrf

from users.models import Course, Student
from profiles.models import*



# Create your views here.



@login_required
def requirements(request):
    return render(request, 'requirements.html')


@login_required
def profiles(request):
    current_student = request.user.student
    list_of_profiles = current_student.profile_set.all()
    context = {'list_of_profiles': list_of_profiles}
    return render(request, 'profiles.html', context)



@login_required
def profile_detail(request, profile_id):
    profile = ()
    return render(request, 'profile_detail.html', {'profile': profile})


@login_required
def view_profiles(request):
    current_student = request.user.student
    list_of_profiles = current_student.profile_set.all()
    context = {'list_of_profiles': list_of_profiles}
    return render(request, 'profiles/view_profiles.html', context)

@login_required
def view_current_profile(request, profile_id):
    # when matching with id, there should only be one match
    # the AUTH_USER_PROFILE is set to the Student model
    current_student = request.user.student
    try:
        current_profile = current_student.profile_set.get(pk=profile_id)
    except Profile.DoesNotExist:
        raise Http404("Profile ID does not exist for this student")

    # Create AU totals
    # TODO: separate into more general function
    # compute total for MAT (NEED TO DO REST)
    math_credits = 0
    ns_credits = 0    # ERROR: this makes the display show up as 0?
    es_credits = 0
    ed_credits = 0
    cs_credits = 0

    list_of_term = current_profile.term_set.all()
    staging_list ={}

    try:
        for term in list_of_term:
            staging_list.append(term.staginglist_set.all())
            for entry in staging_list:
                 # the type conversion is needed, otherwise this return an annoying python Decimal object
                math_credits += float(entry.course.course_math)
                ns_credits += float(entry.course.course_ns)
                es_credits += float(entry.course.course_es)
                ed_credits += float(entry.course.course_ed)
                cs_credits += float(entry.course.course_cs)
    except:
        cs_credits=cs_credits
    mns_credits = math_credits + ns_credits
    eec_credits = es_credits + ed_credits

    # may want to get these from a table later
    ceabReq = {
        'AU' : 1950,
        'MAT' : 214.5,
        'NS' : 200.0,
        'MNC' : 462,
        'ES' : 247.5,
        'ED' : 247.5,
        'EEC' : 990,
        'CS' : 240
    }
    ceab = {
        'AU' : 1884.7,
        'MAT' : math_credits,
        'NS' : ns_credits,
        'MNC' : mns_credits,
        'ES' : es_credits,
        'ED' : ed_credits,
        'EEC' : eec_credits,
        'CS' : cs_credits
    }
    context = {'list_of_term': list_of_term, 'staging_list':staging_list,'profile_id': profile_id, 'ceab': ceab,
               'ceabReq': ceabReq}
    return render(request, 'profiles/view_current_profile.html', context)

@login_required
def create_profile(request):
    current_student = request.user.student
    # add the profile
    new_profile = Profile()
    new_profile.profile_name = 'Another new profile'
    new_profile.student = current_student
    new_profile.save()
    # display the updated list
    #return HttpResponseRedirect(reverse('profiles:view_profiles'))
    return HttpResponseRedirect(reverse('profiles:profiles'))

@login_required
def delete_profile(request, profile_id):
    current_student = request.user.student
    # delete the profile
    profile_to_delete = current_student.profile_set.get(pk=profile_id)
    profile_to_delete.delete()
    # display the updated list
    #return HttpResponseRedirect(reverse('profiles:view_profiles'))
    return HttpResponseRedirect(reverse('profiles:profiles'))

@login_required
def create_term(request,profile_id):
    current_student = request.user.student
    try:
        current_profile = current_student.profile_set.get(pk=profile_id)
    except Profile.DoesNotExist:
        raise Http404("Profile ID does not exist for this student")
    new_term = Term()
    new_term.year = 2010
    new_term.season ='FALL'
    new_term.profile = current_profile
    new_term.save()
    return HttpResponseRedirect(reverse('profiles:view_current_profile',args=(profile_id,)))

