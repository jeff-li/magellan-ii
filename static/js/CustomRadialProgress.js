
    var au=d3.select(document.getElementById('AU'));
    var mat=d3.select(document.getElementById('MAT'));
    var ns=d3.select(document.getElementById('NS'));
    var mnc=d3.select(document.getElementById('MNC'));
    var es=d3.select(document.getElementById('ES'));
    var ed=d3.select(document.getElementById('ED'));
    var eec=d3.select(document.getElementById('EEC'));
    var cs=d3.select(document.getElementById('CS'));

    start();

    function labelFunction(val,min,max) {

    }

    function deselect() {
        au.attr("class","radial");
        mat.attr("class","radial");
        ns.attr("class","radial");
        mnc.attr("class","radial");
        es.attr("class","radial");
        ed.attr("class","radial");
        eec.attr("class","radial");
        cs.attr("class","radial");

    }

    function start() {

        var rp1 = radialProgress(document.getElementById('AU'))
                .label("Total Accreditation Units")
                .diameter(175)
                .value(78)
                .render();

        var rp2 = radialProgress(document.getElementById('MAT'))
                .label("Mathematics")
                .diameter(175)
                .value(132)
                .render();

        var rp3 = radialProgress(document.getElementById('NS'))
                .label("Natural Science")
                .diameter(175)
                .minValue(100)
                .maxValue(200)
                .value(150)
                .render();

        var rp4 = radialProgress(document.getElementById('MNC'))
                .label("MAT and NS Combined")
                .diameter(175)
                .minValue(100)
                .maxValue(200)
                .value(150)
                .render();

        var rp5 = radialProgress(document.getElementById('ES'))
                .label("Engineering Science")
                .diameter(175)
                .value(78)
                .render();

        var rp6 = radialProgress(document.getElementById('ED'))
                .label("Engineering Design")
                .diameter(175)
                .value(78)
                .render();

        var rp7 = radialProgress(document.getElementById('EEC'))
                .label("ES and ED Combined")
                .diameter(175)
                .value(78)
                .render();

        var rp8 = radialProgress(document.getElementById('CS'))
                .label("Complementary Studies")
                .diameter(175)
                .value(78)
                .render();

    }

    