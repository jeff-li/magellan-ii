$(function(){
    $('#search').keyup(function(){ 
        $.ajax({
            type: "POST",
            url: "/courses/s/",
            data: {
                'search_text': $('#search').val(),
                'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()
            },
            success: function (data, textStatus, jqXHR) {
            $('#search-results').html(data)
        },
            dataType: 'html'
        });
    });
});