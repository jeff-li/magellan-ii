from django.contrib import admin
from users.models import Course, Student

class StudentAdmin(admin.ModelAdmin):
	list_display = ('student_name', 'student_number')

class CourseAdmin(admin.ModelAdmin):
	list_display = ('course_code', 
					'course_term',
					'course_title', 
					'course_description', 
					'course_prerequisites',
					'course_corequisites',
					'course_exclusions',
					'course_credit_weight',
					'course_math',
					'course_ns',
					'course_cs',
					'course_es',
					'course_ed',
					'course_area')

admin.site.register(Course, CourseAdmin)
admin.site.register(Student, StudentAdmin)
