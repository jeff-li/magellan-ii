# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('course_code', models.CharField(max_length=10)),
                ('course_term', models.CharField(default=b'', max_length=2)),
                ('course_title', models.CharField(max_length=100)),
                ('course_description', models.CharField(default=b'None', max_length=1000)),
                ('course_prerequisites', models.CharField(default=b'None', max_length=50)),
                ('course_corequisites', models.CharField(default=b'None', max_length=50)),
                ('course_exclusions', models.CharField(default=b'None', max_length=50)),
                ('course_credit_weight', models.DecimalField(default=0.5, max_digits=5, decimal_places=2)),
                ('course_math', models.DecimalField(default=0.0, max_digits=5, decimal_places=2)),
                ('course_ns', models.DecimalField(default=0.0, max_digits=5, decimal_places=2)),
                ('course_cs', models.DecimalField(default=0.0, max_digits=5, decimal_places=2)),
                ('course_es', models.DecimalField(default=0.0, max_digits=5, decimal_places=2)),
                ('course_ed', models.DecimalField(default=0.0, max_digits=5, decimal_places=2)),
                ('course_area', models.CharField(default=b'0', max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('student_name', models.CharField(max_length=200)),
                ('student_number', models.IntegerField(default=0)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
