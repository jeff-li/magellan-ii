from django.db import models
from django.db.models.signals import post_save 
from django.dispatch import receiver
from django.contrib.auth.models import User


class Course(models.Model):
    course_code = models.CharField(max_length=10)
    course_term = models.CharField(max_length=2, default="")
    course_title = models.CharField(max_length=100)
    course_description = models.CharField(max_length=1000, default="None")
    course_prerequisites = models.CharField(max_length=50, default="None")
    course_corequisites = models.CharField(max_length=50, default="None")
    course_exclusions = models.CharField(max_length=50, default="None")
    course_credit_weight = models.DecimalField(max_digits=5, decimal_places=2, default=0.5)
    course_math = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
    course_ns = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
    course_cs = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
    course_es = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
    course_ed = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
    course_area = models.CharField(max_length=50, default="0")

    def __unicode__(self):
        return self.course_code


class Student(models.Model):
    user = models.OneToOneField(User)
    student_name = models.CharField(max_length=200)
    student_number = models.IntegerField(default=0)
    # courses_taken = models.ManyToManyField(Course, blank=True)

    def __unicode__(self):
        return self.student_name

@receiver(post_save, sender=User)
def create_student(sender, instance, created, **kwargs):
    if created:
        new_student = Student()
        new_student.student_name = "New Student"
        new_student.user = instance
        new_student.save()
    

#User.student = property(lambda u: Student.objects.get_or_create(user=u)[0])
