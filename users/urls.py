from django.conf.urls import patterns, url

from users import views

urlpatterns = patterns('',
    url(r'^edit', views.edit_user_info, name='edit_user_info')
)