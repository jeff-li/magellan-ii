from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect

from users.models import Course, Student


def index(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/home')
    return render(request, 'index.html')


@login_required
def home(request):
    user = request.user

    ceabReq = {
        'AU' : 1950,
        'MAT' : 214.5,
        'NS' : 200,
        'MNC' : 462,
        'ES' : 247.5,
        'ED' : 247.5,
        'EEC' : 990,
        'CS' : 240
    }

    ceab = {
        'AU' : 1884.7,
        'MAT' : 262.6,
        'NS' : 251.5,
        'MNC' : 514.1,
        'ES' : 764.3,
        'ED' : 356.4,
        'EEC' : 1120.7,
        'CS' : 249.9
    }

    context = {'user': user, 'ceab':ceab, 'ceabReq': ceabReq}
    return render(request, 'home.html', context)


@login_required
def edit_user_info(request):
    user = get_object_or_404(Student, pk=request.user.id)
    return render(request, 'edit_user_info.html', {'user': user})